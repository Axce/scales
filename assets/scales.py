import sys

while True:
    strin = input().split()
    if len(strin) == 0:
        continue
    #print(strin)

    out = ["i1","pm","p","pm","p","pt","pt","pt","pm","p","pm","p","i8"]
    #print(out)

    for i in strin :
        #   0   1   2   3   4   5   6   7   8   9   10  11  12
        #   1   2m  2   3m  3   4   5-  5   6m  6   7m  7   8

        # diminués et augmentés

        if i == "2+" :
            out[2] = "pc"
            out[3] = "i2a"
        if i == "3-" :
            out[2] = "i3d"
            out[3] = "pc"
        if i == "4-" :
            out[4] = "i4d"
            out[5] = "p"
        if i == "4+" :
            out[5] = "pm"
            out[6] = "i4a"
        if i == "5-" :
            out[7] = "p"
            out[6] = "i5d"
        if i == "5+" :
            out[7] = "pm"
            out[8] = "i5a"
        if i == "6+" :
            out[10] = "i6a"
            out[9] = "pc"
        if i == "7-" :
            out[9] = "i7d"
            out[10] = "pc"

        # normaux

        if i == "2m" :
            out[1] = "i2m"  
        if i == "2" :
            out[2] = "i2"
        if i == "3m" :
            out[3] = "i3m"
        if i == "3" :
            out[4] = "i3"
        if i == "4" :
            out[5] = "i4"
        if i == "5" :
            out[7] = "i5"
        if i == "6m" :
            out[8] = "i6m"
        if i == "6" :
            out[9] = "i6"
        if i == "7m" :
            out[10] = "i7m"
        if i == "7" :
            out[11] = "i7"
        
    #print(out)



    print('        <ul class="common scale">', end='')
    print(' <h2>' + strin[0] + '</h2>', end='')
    print(' <li class="' + out[0] + '">', end='')
    print(' <li class="' + out[1] + '">', end='')
    print(' <li class="' + out[2] + '">', end='')
    print(' <li class="' + out[3] + '">', end='')
    print(' <li class="' + out[4] + '">', end='')
    print(' <li class="' + out[5] + '">', end='')
    print(' <li class="' + out[6] + '">', end='')
    print(' <li class="' + out[7] + '">', end='')
    print(' <li class="' + out[8] + '">', end='')
    print(' <li class="' + out[9] + '">', end='')
    print(' <li class="' + out[10] + '">', end='')
    print(' <li class="' + out[11] + '">', end='')
    print(' <li class="' + out[12] + '">', end='')
    print(' </ul>')



